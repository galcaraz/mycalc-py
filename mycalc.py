#!/bin/python3

#código por Guillermo Alcaraz

from operator import *
from sys import *
from decimal import *

if len(argv) != 4 :
	print("Input error: $mycalc.py OPERATOR ARG1 ARG2")
else:
	try:
		A=Decimal(argv[2])
		B=Decimal(argv[3])
	except decimal.InvalidOperation as e:
		print("Input error: Need 2 int or float ARGS: {}".format(e), file=stderr)
		exit()
		
Dic={
	'suma': add,
	'resta': sub,
	'multiplicacion': mul,
	'division': truediv
}

try:
	print(Dic[argv[1]](A, B))
except DivisionByZero :
	print("Cant divide by 0", file=stderr)